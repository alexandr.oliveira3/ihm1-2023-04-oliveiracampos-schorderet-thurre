# Modifications par rapport à la maquette

- Affichage des tags sur les tâches : utiliser un style avec un contour au lieu d'un point. Permet d'unifier le style.
- Changement des boutons sur la tâche : Uniformisation mobile et Desktop en utilisant des boutons avec du texte à côté
- Ajout de tache : Suppression du bouton annuler pour simplifier l'interface, il suffit de cliquer à côté pour quitter
- Retrait de la page "Historique" : La user story ne la mentionnait pas, cela simplifie le nombre de fonctions
- Design général : Certaines différences de designs sont influencéess par le style de Vuetify
- Retrait du bouton notifications : La fonction de notification est directement liée à la date d'échéance, les deux boutons sont redondants
- L'icône pour le menu "Projet" : Elle a été remplacée par une icône de dossier pour des soucis d'intuitivité
- Le bouton "modifier une tâche" n'est pas implémenté, mais le but est d'ouvrir une modal avec un formulaire avec les champs remplis des données de la tâche