// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Vuetify
import { createVuetify } from 'vuetify'

const darkTheme = {
  dark: true,
  colors: {
    primary: '#864ED4',
    secondary: '#FF5353',
    surface: '#1F1E20',
    background: '#020006'
  },
};

export default createVuetify({
  // https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
  theme: {
    defaultTheme: 'darkTheme',
    themes: {
      darkTheme,
    },
  },
});

